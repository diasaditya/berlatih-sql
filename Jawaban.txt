1. Buat Database

Create Database myshop;

2. Membuat Table didalam Database

tabel users
create table users (
	id INT AUTO_INCREMENT PRIMARY KEY, 
	name varchar(255) not null, 
	email  varchar(255) not null, 
	password varchar(255) not null
);

table categories
create table categories (
	id int auto_increment primary key,
	name varchar(255) not null
);

table items
create table items (
	id int auto_increment primary key,
	name varchar(255) not null,
	description varchar(255) not null,
	price int not null,
	stock int not null,
	category_id int not null,
	foreign key (category_id)
	references categories(id)
);

3. Memasukan Data Pada Table
masukkan data kedalam tabel users
insert into users (name, email, password) values
	("Jhon Doe", "jhon@doe.com", "john123"),
	("Jane Doe", "jane@doe.com", "jenita123");


masukkan data kedalam tabel categories
insert into categories (name) values
	("gadget"),
	("cloth"),
	("men"),
	("women"),
	("branded");


masukkan data kedalam tabel items
insert into items (name, description, price, stock, category_id) values 
	("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
	("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
	("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil Data Dari Database

A. Mengambil Data users

select id, name, email from users;

B. Mengambil Data items

select * from items where price > 1000000;

select * from items where name like "%uniklo%";

C. Menampilkan Data Items Join dengan Kategori

select a.name, a.description, a.price, a.stock, a.category_id, b.name as 'kategori'
	from items as a 
	inner join categories as b ON a.category_id = b.id;


5. Mengubah Data Database

update items
	set price = 2500000
	where name = "sumsang b50";
